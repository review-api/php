<?php

namespace Tests\Unit\Entity;

use App\Abstracts\EntityAbstract;
use App\Entity\Hotel;
use PHPUnit\Framework\TestCase;

/**
 * Class HotelTest
 *
 * @package Tests\Unit\Entity
 */
class HotelTest extends TestCase
{
    public function testObject()
    {
        $obj = new Hotel();
        $this->assertInstanceOf(EntityAbstract::class, $obj);
        $this->assertTrue(method_exists($obj, 'setterByName'));
    }

    /**
     * @dataProvider attributesDataProvider
     *
     * @param $attributes
     */
    public function testObjectCreate($attributes)
    {
        $obj = new Hotel($attributes);
        isset($attributes['name'])
            ? $this->assertEquals($attributes['name'], $obj->getName())
            : $this->assertEmpty($obj->getName());
    }

    public function attributesDataProvider()
    {
        return [
            [['' => 'abc']],
            [['a' => 'abc', 'b' => 123]],
            [['name' => 'name 1', 'c' => 'def']],
            [['d' => 'gij', 'name' => 'name 2']],
            [['name' => 'gij']],
        ];
    }

    /**
     * @dataProvider nameDataProvider
     *
     * @param $name
     * @param $expectedSetterName
     */
    public function testSetterByNameMethod($name, $expectedSetterName)
    {
        $obj = new Hotel();
        $this->assertEquals($expectedSetterName, $obj::setterByName($name));
    }

    public function nameDataProvider()
    {
        return [
            ['', 'set'],
            ['abc', 'setAbc'],
            ['abc_def', 'setAbcDef'],
        ];
    }
}
