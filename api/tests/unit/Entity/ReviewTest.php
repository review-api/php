<?php

namespace Tests\Unit\Entity;

use App\Abstracts\EntityAbstract;
use App\Entity\Review;
use DateInterval;
use DateTime;
use PHPUnit\Framework\TestCase;

/**
 * Class ReviewTest
 *
 * @package Tests\Unit\Entity
 */
class ReviewTest extends TestCase
{
    public function testObject()
    {
        $obj = new Review();
        $this->assertInstanceOf(EntityAbstract::class, $obj);
        $this->assertTrue(method_exists($obj, 'setterByName'));
    }

    /**
     * @dataProvider attributesDataProvider
     *
     * @param $attributes
     */
    public function testObjectCreate($attributes)
    {
        $obj = new Review($attributes);

        isset($attributes['comment'])
            ? $this->assertEquals($attributes['comment'], $obj->getComment())
            : $this->assertEmpty($obj->getComment());
        isset($attributes['score'])
            ? $this->assertEquals($attributes['score'], $obj->getScore())
            : $this->assertEmpty($obj->getScore());
    }

    public function attributesDataProvider()
    {
        return [
            [['' => 'abc']],
            [['a' => 'abc', 'b' => 123]],
            [['comment' => 'comment 1', 'c' => 'def']],
            [['d' => 'gij', 'comment' => 'comment 2']],
            [['score' => 100, 'c' => 'def']],
            [['d' => 'gij', 'score' => 200]],
            [['comment' => 'gij', 'score' => 20]],
        ];
    }

    /**
     * @dataProvider existingAttributesDataProvider
     *
     * @param $attributes
     */
    public function testObjectCreateSetters($attributes)
    {
        $now = new DateTime();
        $obj = new Review($attributes);

        isset($attributes['created_date'])
            ? $this->assertEquals($attributes['created_date']->format('Y-m-d'), $obj->getCreatedDate()->format('Y-m-d'))
            : $this->assertEquals($now->format('Y-m-d'), $obj->getCreatedDate()->format('Y-m-d'));
        isset($attributes['comment'])
            ? $this->assertEquals($attributes['comment'], $obj->getComment())
            : $this->assertEmpty($obj->getComment());
        isset($attributes['score'])
            ? $this->assertEquals($attributes['score'], $obj->getScore())
            : $this->assertEmpty($obj->getScore());
    }

    public function existingAttributesDataProvider()
    {
        $now = new DateTime();

        return [
            [['comment' => 'comment 1', 'c' => 'def', 'created_date' => (clone $now)->sub(new DateInterval('P2D')),]],
            [['d' => 'gij', 'comment' => 'comment 2']],
            [['score' => 10, 'comment' => 'def', 'created_date' => (clone $now)->sub(new DateInterval('P1D')),]],
            [['comment' => 'gij', 'score' => 2]],
        ];
    }

    /**
     * @dataProvider nameDataProvider
     *
     * @param $name
     * @param $expectedSetterName
     */
    public function testSetterByNameMethod($name, $expectedSetterName)
    {
        $obj = new Review();
        $this->assertEquals($expectedSetterName, $obj::setterByName($name));
    }

    public function nameDataProvider()
    {
        return [
            ['', 'set'],
            ['abc', 'setAbc'],
            ['abc_def', 'setAbcDef'],
        ];
    }

    /**
     * @dataProvider propertyNameDataProvider
     *
     * @param $name
     * @param $expectedPropertyName
     */
    public function testPropertyNameMethod($name, $expectedPropertyName)
    {
        $obj = new Review();
        $this->assertEquals($expectedPropertyName, $obj::propertyName($name));
    }

    public function propertyNameDataProvider()
    {
        return [
            ['', ''],
            ['abc', 'abc'],
            ['abc_def', 'abcDef'],
            ['abc_def_ghi', 'abcDefGhi'],
        ];
    }
}
