<?php

namespace Tests\Api\Controller;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Repository\HotelRepository;
use App\Repository\ReviewRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Swagger\Server\Model\OvertimeApiResponse;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ReviewControllerTest
 *
 * @package Tests\Api\Controller
 */
class ReviewControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    private KernelBrowser $client;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var ReviewRepository
     */
    private $repo;
    /**
     * @var HotelRepository
     */
    private $hotelRepo;
    /**
     * @var Hotel
     */
    private $hotel;
    /**
     * @var Serializer
     */
    private Serializer $serializer;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->em         = static::bootKernel()->getContainer()
                                  ->get('doctrine')
                                  ->getManager();
        $this->repo       = $this->em->getRepository(Review::class);
        $this->hotelRepo  = $this->em->getRepository(Hotel::class);
        $this->serializer = SerializerBuilder::create()->build();

        $this->em
            ->createQueryBuilder()
            ->delete(Hotel::class, 'h')
            ->getQuery()
            ->execute();
        $this->em
            ->createQueryBuilder()
            ->delete(Review::class, 'r')
            ->getQuery()
            ->execute();

        $this->hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);
    }

    public function testGetActionOk()
    {
        $score   = 9;
        $comment = 'Review #1';

        $obj = $this->repo->create([
            'hotel'   => $this->hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);

        $this->client->request('GET', '/review/' . $obj->getId());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            sprintf(
                '{"code":2000200,"message":"review found","data":{"id":%d,"score":%d,"comment":"%s"}}',
                $obj->getId(),
                $score,
                $comment
            ),
            $this->client->getResponse()->getContent()
        );
    }

    /**
     * @dataProvider getActionFailDataProvider
     *
     * @param $id
     * @param $expectedStatus
     */
    public function testGetActionFail($id, $expectedStatus)
    {
        $this->client->request('GET', '/review/' . $id);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function getActionFailDataProvider()
    {
        return [
            [0, 400],
            ['', 404],
            ['not-exists', 400],
        ];
    }

    public function testPostActionOk()
    {
        $score   = 9;
        $comment = 'Review #1';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('POST', '/review', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'hotel_id' => $this->hotel->getId(),
            'score'    => $score,
            'comment'  => $comment,
        ]));
        $obj = $this->repo->findOneBy(['comment' => $comment]);

        $this->assertNotEmpty($obj);
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPostActionFail($content, $expectedStatus)
    {
        $this->client->request('POST', '/review', [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function postActionFailDataProvider()
    {
        return [
            [0, 400],
            ['', 400],
            ['not-exists', 400],
        ];
    }

    public function testPatchActionOk()
    {
        $score         = 9;
        $comment       = 'Review #1 for patching';
        $obj           = $this->repo->create([
            'hotel'   => $this->hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);
        $updateScore   = 5;
        $updateComment = 'Review #1 patched';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('PATCH', '/review/' . $obj->getId(), [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'score'   => $updateScore,
                'comment' => $updateComment,
            ]));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPatchActionFail($content, $expectedStatus)
    {
        $score   = 9;
        $comment = 'Review #1 for patching';
        $obj     = $this->repo->create([
            'hotel'   => $this->hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);

        $this->client->request('PATCH', '/review/' . $obj->getId(), [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"code":2000400,"message":"bad request"}', $this->client->getResponse()->getContent());
    }

    public function testPutActionOk()
    {
        $score         = 9;
        $comment       = 'Review #1 for put update';
        $obj           = $this->repo->create([
            'hotel'   => $this->hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);
        $updateScore   = 2;
        $updateComment = 'Review #1 updated';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('PUT', '/review/' . $obj->getId(), [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'hotel_id' => $this->hotel->getId(),
                'score'    => $updateScore,
                'comment'  => $updateComment,
            ]));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPutActionFail($content, $expectedStatus)
    {
        $score   = 9;
        $comment = 'Review #1 for put update';
        $obj     = $this->repo->create([
            'hotel'   => $this->hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);

        $this->client->request('PUT', '/review/' . $obj->getId(), [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"code":2000400,"message":"bad request"}', $this->client->getResponse()->getContent());
    }


    public function testDeleteActionOk()
    {
        $score   = 1;
        $comment = 'Review #1';
        $obj     = $this->repo->create([
            'hotel'   => $this->hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);
        $id      = $obj->getId();

        $this->client->request('DELETE', '/review/' . $id);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            sprintf(
                '{"code":2000200,"message":"review found","data":{"score":%d,"comment":"%s"}}',
                $score,
                $comment
            ),
            $this->client->getResponse()->getContent()
        );

        $obj = $this->repo->findOneBy(['id' => $id]);
        $this->assertNull($obj);
    }

    /**
     * @dataProvider getActionFailDataProvider
     *
     * @param $id
     * @param $expectedStatus
     */
    public function testDeleteActionFail($id, $expectedStatus)
    {
        $this->client->request('DELETE', '/review/' . $id);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider searchActionOkDataProvider
     *
     * @param array $search
     * @param int   $expectedStatus
     *
     * @throws ORMException
     */
    public function testSearchActionOk(array $search, int $expectedStatus)
    {
        $attributes          = $search;
        $attributes['hotel'] = $this->hotel;
        if (!isset($search['score'])) {
            $attributes['score'] = 7;
        }
        if (!isset($search['comment'])) {
            $attributes['comment'] = 'added comment';
        }

        $this->repo->create($attributes);

        $this->client->request('POST', '/review/search', [], [], [], json_encode($search));

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function searchActionOkDataProvider()
    {
        return [
            [['comment' => 'review #1 comment'], 200],
            [['score' => 1], 200],
            [['abc' => 'def'], 404],
            [['score' => 1, 'abc' => 'def', 'comment' => 'review #1 comment'], 200],
        ];
    }

    /**
     * @dataProvider overtimeDayDataProvider
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param array    $expectedTotals
     *
     * @throws ORMException
     */
    public function testOvertimeAction(DateTime $startDate, DateTime $endDate, array $expectedTotals)
    {
        // create all
        $fixturesData = $this->overtimeReviewsDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'hotel'        => $this->hotel,
                'score'        => $data[0],
                'comment'      => $data[1],
                'created_date' => $data[2],
            ]);
        }

        //do call
        $response = $this->doRequest($startDate, $endDate);

        $this->assertCount($expectedTotals[ReviewRepository::DATE_GROUP_DAY], $response->getData()->getDay());
        $this->assertCount($expectedTotals[ReviewRepository::DATE_GROUP_WEEK], $response->getData()->getCalendarWeek());
        $this->assertCount($expectedTotals[ReviewRepository::DATE_GROUP_MONTH], $response->getData()->getMonth());
    }

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     *
     * @return OvertimeApiResponse
     */
    protected function doRequest(DateTime $startDate, DateTime $endDate): OvertimeApiResponse
    {
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request(
            'GET',
            sprintf(
                '/overtime/%d/%s/%s',
                $this->hotel->getId(),
                $startDate->format('Y-m-d'),
                $endDate->format('Y-m-d'))
        );
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        return $this->serializer->deserialize(
            $this->client->getResponse()->getContent(),
            OvertimeApiResponse::class, 'json'
        );
    }

    public function overtimeDayDataProvider()
    {
        $now = new DateTime();

        return [
            // day
            'current day'                    => [
                $now,
                $now,
                [
                    ReviewRepository::DATE_GROUP_DAY   => 1,
                    ReviewRepository::DATE_GROUP_WEEK  => 0,
                    ReviewRepository::DATE_GROUP_MONTH => 0,
                ]
            ],
            '1 day range'                    => [
                (clone $now)->sub(new DateInterval('P1D')),
                $now,
                [
                    ReviewRepository::DATE_GROUP_DAY   => 2,
                    ReviewRepository::DATE_GROUP_WEEK  => 0,
                    ReviewRepository::DATE_GROUP_MONTH => 0,
                ]
            ],
            'max day range(29), set more'    => [
                (clone $now)->sub(new DateInterval('P40D')),
                $now,
                [
                    ReviewRepository::DATE_GROUP_DAY   => 4,
                    ReviewRepository::DATE_GROUP_WEEK  => 1,
                    ReviewRepository::DATE_GROUP_MONTH => 0,
                ]
            ],

            // week
            '40 days in range(30 - 89 days)' => [
                (clone $now)->sub(new DateInterval('P40D')),
                $now,
                [
                    ReviewRepository::DATE_GROUP_DAY   => 4,
                    ReviewRepository::DATE_GROUP_WEEK  => 1,
                    ReviewRepository::DATE_GROUP_MONTH => 0,
                ]
            ],
            'max days range(30 - 89 days)'   => [
                (clone $now)->sub(new DateInterval('P100D')),
                $now,
                [
                    ReviewRepository::DATE_GROUP_DAY   => 4,
                    ReviewRepository::DATE_GROUP_WEEK  => 2,
                    ReviewRepository::DATE_GROUP_MONTH => 1,
                ]
            ],

            //month
            'max range(>89 days)'            => [
                (clone $now)->sub(new DateInterval('P2Y')),
                $now,
                [
                    ReviewRepository::DATE_GROUP_DAY   => 4,
                    ReviewRepository::DATE_GROUP_WEEK  => 2,
                    ReviewRepository::DATE_GROUP_MONTH => 2,
                ]
            ],
        ];
    }

    public function overtimeReviewsDataProvider()
    {
        $now = new DateTime();

        return [
            'review-1-now'         => [
                10,
                'review #1',
                $now,
            ],
            'review-2-yesterday-1' => [
                2,
                'review #2.1',
                (clone $now)->sub(new DateInterval('P1D')),
            ],
            'review-2-yesterday-2' => [
                2,
                'review #2.2',
                (clone $now)->sub(new DateInterval('P1D')),
            ],
            'review-3-p2d'         => [
                3,
                'review #3',
                (clone $now)->sub(new DateInterval('P2D')),
                $now,
            ],
            'review-4-p29d'        => [
                4,
                'review #4',
                (clone $now)->sub(new DateInterval('P29D')),
            ],
            'review-5-p30d'        => [
                5,
                'review #5',
                (clone $now)->sub(new DateInterval('P30D')),
            ],
            'review-6-p89d'        => [
                6,
                'review #6',
                (clone $now)->sub(new DateInterval('P89D')),
            ],
            'review-7-p90d'        => [
                7,
                'review #7',
                (clone $now)->sub(new DateInterval('P90D')),
            ],
            'review-8-p120d'       => [
                8,
                'review #8',
                (clone $now)->sub(new DateInterval('P120D')),
            ],
        ];
    }
}
