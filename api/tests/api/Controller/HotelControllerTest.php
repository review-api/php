<?php

namespace Tests\Api\Controller;

use App\Entity\Hotel;
use App\Repository\HotelRepository;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class HotelControllerTest
 *
 * @package Tests\Api\Controller
 */
class HotelControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    private KernelBrowser $client;
    /**
     * @var HotelRepository
     */
    private $repo;
    /**
     * @var Serializer
     */
    private Serializer $serializer;

    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->repo       = static::bootKernel()->getContainer()
                                  ->get('doctrine')
                                  ->getManager()
                                  ->getRepository(Hotel::class);
        $this->serializer = SerializerBuilder::create()->build();

        static::bootKernel()->getContainer()
              ->get('doctrine')
              ->getManager()
              ->createQueryBuilder()
              ->delete(Hotel::class, 'r')
              ->getQuery()
              ->execute();
    }

    public function testGetActionOk()
    {
        $name = 'Hotel #1';
        $obj  = $this->repo->create([
            'name' => $name,
        ]);
        static::bootKernel()->getContainer()
              ->get('doctrine')
              ->getManager()->flush();

        $this->client->request('GET', '/hotel/' . $obj->getId());

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            sprintf(
                '{"code":1000200,"message":"hotel found","data":{"id":%d,"name":"%s"}}',
                $obj->getId(),
                $name
            ),
            $this->client->getResponse()->getContent()
        );
    }

    /**
     * @dataProvider getActionFailDataProvider
     *
     * @param $id
     * @param $expectedStatus
     */
    public function testGetActionFail($id, $expectedStatus)
    {
        $this->client->request('GET', '/hotel/' . $id);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function getActionFailDataProvider()
    {
        return [
            [0, 400],
            ['', 404],
            ['not-exists', 400],
        ];
    }

    public function testPostActionOk()
    {
        $name = 'Hotel #1';

        $this->client->request('POST', '/hotel', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode([
            'name' => $name,
        ]));
        $obj = $this->repo->findOneBy(['name' => $name]);

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
        $this->assertNotEmpty($obj);
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPostActionFail($content, $expectedStatus)
    {
        $this->client->request('POST', '/hotel', [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function postActionFailDataProvider()
    {
        return [
            [0, 400],
            ['', 400],
            ['not-exists', 400],
        ];
    }

    public function testPatchActionOk()
    {
        $name       = 'Hotel #1 for patching';
        $obj        = $this->repo->create([
            'name' => $name,
        ]);
        $updateName = 'Hotel #1 patched';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('PATCH', '/hotel/' . $obj->getId(), [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name' => $updateName,
            ]));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPatchActionFail($content, $expectedStatus)
    {
        $name = 'Hotel #1 for patching';
        $obj  = $this->repo->create([
            'name' => $name,
        ]);

        $this->client->request('PATCH', '/hotel/' . $obj->getId(), [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"code":1000400,"message":"bad request"}', $this->client->getResponse()->getContent());
    }

    public function testPutActionOk()
    {
        $name       = 'Hotel #1 for put update';
        $obj        = $this->repo->create([
            'name' => $name,
        ]);
        $updateName = 'Hotel #1 updated';

        $this->client->setServerParameter('CONTENT_TYPE', 'application/json');
        $this->client->request('PUT', '/hotel/' . $obj->getId(), [], [], ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name' => $updateName,
            ]));

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider postActionFailDataProvider
     *
     * @param $content
     * @param $expectedStatus
     */
    public function testPutActionFail($content, $expectedStatus)
    {
        $name = 'Hotel #1 for put update';
        $obj  = $this->repo->create([
            'name' => $name,
        ]);

        $this->client->request('PUT', '/hotel/' . $obj->getId(), [], [], [], $content);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
        $this->assertEquals('{"code":1000400,"message":"bad request"}', $this->client->getResponse()->getContent());
    }

    public function testDeleteActionOk()
    {
        $name = 'Hotel #1';
        $obj  = $this->repo->create([
            'name' => $name,
        ]);
        $id   = $obj->getId();

        $this->client->request('DELETE', '/hotel/' . $id);

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            sprintf(
                '{"code":1000200,"message":"hotel found","data":{"name":"%s"}}',
                $name
            ),
            $this->client->getResponse()->getContent()
        );

        $obj = $this->repo->findOneBy(['id' => $id]);
        $this->assertNull($obj);
    }

    /**
     * @dataProvider getActionFailDataProvider
     *
     * @param $id
     * @param $expectedStatus
     */
    public function testDeleteActionFail($id, $expectedStatus)
    {
        $this->client->request('DELETE', '/hotel/' . $id);

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider searchActionOkDataProvider
     *
     * @param array $search
     * @param int   $expectedStatus
     */
    public function testSearchActionOk(array $search, int $expectedStatus)
    {
        $attributes = $search;
        if (!isset($search['name'])) {
            $attributes['name'] = 'added name';
        }

        $this->repo->create($attributes);
        
        $this->client->request('POST', '/hotel/search', [], [], [], json_encode($search));

        $this->assertEquals($expectedStatus, $this->client->getResponse()->getStatusCode());
    }

    public function searchActionOkDataProvider()
    {
        return [
            [['name' => 'hotel name'], 200],
            [['abc' => 'def'], 404],
            [['abc' => 'def', 'name' => 'hotel name'], 200],
        ];
    }
}
