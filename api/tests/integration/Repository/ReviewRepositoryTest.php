<?php

namespace Tests\Integration\Repository;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Repository\HotelRepository;
use App\Repository\ReviewRepository;
use DateInterval;
use DateTime;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class ReviewRepositoryTest
 *
 * @package Tests\Integration\Repository
 */
class ReviewRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ReviewRepository $repo
     */
    private $repo;

    /**
     * @var HotelRepository $hotelRepo
     */
    private $hotelRepo;

    protected function setUp(): void
    {
        $kernel          = self::bootKernel();
        $this->em        = $kernel->getContainer()
                                  ->get('doctrine')
                                  ->getManager();
        $this->hotelRepo = $this->em->getRepository(Hotel::class);
        $this->repo      = $this->em->getRepository(Review::class);

        $this->em->createQueryBuilder()
                 ->delete(Hotel::class, 'h')
                 ->getQuery()
                 ->execute();
        $this->em->createQueryBuilder()
                 ->delete(Review::class, 'r')
                 ->getQuery()
                 ->execute();
    }

    /**
     * @dataProvider createReviewDataProvider
     *
     * @param $score
     * @param $comment
     *
     * @throws ORMException
     */
    public function testCreateMethodOk($score, $comment)
    {
        $obj = $this->repo->create([
            'hotel'   => $this->hotelRepo->create([
                'name' => md5(random_bytes(120)),
            ]),
            'score'   => $score,
            'comment' => $comment,
        ]);

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->getId());
        $this->assertInstanceOf(Review::class, $obj);
        $this->assertEquals($score, $obj->getScore());
        $this->assertEquals($comment, $obj->getComment());
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function createReviewDataProvider()
    {
        return [
            'rand-comment-1' => [
                0,
                md5(random_bytes(32)),
            ],
            'rand-comment-2' => [
                10,
                md5(random_bytes(32)),
            ],
            'rand-comment-3' => [
                5,
                md5(random_bytes(32)),
            ],
        ];
    }

    /**
     * @dataProvider createReviewFailDataProvider
     *
     * @param $attributes
     *
     * @throws ORMException
     */
    public function testCreateMethodFail($attributes)
    {
        $this->expectException(NotNullConstraintViolationException::class);

        $this->repo->create($attributes);
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function createReviewFailDataProvider()
    {
        return [
            'no-input'                => [null],
            'partly-missing: score'   => [['comment' => 'a comment']],
            'partly-missing: comment' => [['score' => 9]],
        ];
    }


    /**
     * @dataProvider updateReviewDataProvider
     *
     * @param $score
     * @param $comment
     * @param $updateScore
     * @param $updateComment
     *
     * @throws ORMException
     */
    public function testUpdateMethodOk($score, $comment, $updateScore, $updateComment)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        //create new
        $obj = $this->repo->create([
            'hotel'   => $hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(Review::class, $obj);
        $this->assertEquals($score, $obj->getScore());
        $this->assertEquals($comment, $obj->getComment());

        //update
        $obj = $this->repo->update(
            $obj,
            [
                'hotel'   => $hotel,
                'score'   => $updateScore,
                'comment' => $updateComment,
            ]
        );

        $this->assertEquals($id, $obj->getId());
        $this->assertEquals($updateScore, $obj->getScore());
        $this->assertEquals($updateComment, $obj->getComment());
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function updateReviewDataProvider()
    {
        return [
            // score, comment, update score, update comment
            'rand-comment-1' => [
                0,
                md5(random_bytes(32)),
                2,
                md5(random_bytes(32)),
            ],
            'rand-comment-2' => [
                10,
                md5(random_bytes(32)),
                5,
                md5(random_bytes(32)),
            ],
            'rand-comment-3' => [
                7,
                md5(random_bytes(32)),
                9,
                md5(random_bytes(32)),
            ],
        ];
    }

    /**
     * @dataProvider createReviewFailDataProvider
     *
     * @param $attributes
     *
     * @throws ORMException
     */
    public function testUpdateMethodPartlyOk($attributes)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        $obj = $this->repo->create([
            'hotel'   => $hotel,
            'score'   => 3,
            'comment' => 'created review',
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(Review::class, $obj);

        $obj = $this->repo->update($obj, $attributes);

        $this->assertEquals($id, $obj->getId());
        if (isset($attributes['score'])) {
            $this->assertEquals($attributes['score'], $obj->getScore());
        }
        if (isset($attributes['comment'])) {
            $this->assertEquals($attributes['comment'], $obj->getComment());
        }
    }

    /**
     * @dataProvider createReviewDataProvider
     *
     * @param $score
     * @param $comment
     *
     * @throws ORMException
     */
    public function testDeleteMethodOk($score, $comment)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        $obj = $this->repo->create([
            'hotel'   => $hotel,
            'score'   => $score,
            'comment' => $comment,
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->getId());
        $this->assertInstanceOf(Review::class, $obj);

        $this->repo->delete($obj);

        $this->assertNull($this->repo->find($id));
    }

    /**
     * @throws ORMException
     */
    public function testDeleteMethodFail()
    {
        $this->expectException(ORMInvalidArgumentException::class);

        $this->repo->delete(new Review(['id' => -1]));
    }


    /**
     * @dataProvider searchReviewDataProvider
     *
     * @param $score
     * @param $comment
     *
     * @throws ORMException
     * @throws Exception
     */
    public function testSearchMethodOk($score, $comment)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        // create all
        $fixturesData = $this->searchReviewDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'hotel'   => $hotel,
                'score'   => $data[0],
                'comment' => $data[1],
            ]);
        }

        //search existing one
        $found = $this->repo->search(['score' => $score]);
        $id    = $found[0]->getId();

        $this->assertNotEmpty($found);
        $this->assertCount(1, $found);
        $this->assertNotEmpty($found[0]);
        $this->assertNotEmpty($found[0]->getId());
        $this->assertInstanceOf(Review::class, $found[0]);
        $this->assertEquals($score, $found[0]->getScore());

        $found = $this->repo->search(['comment' => $comment]);

        $this->assertNotEmpty($found);
        $this->assertCount(1, $found);
        $this->assertNotEmpty($found[0]);
        $this->assertNotEmpty($found[0]->getId());
        $this->assertInstanceOf(Review::class, $found[0]);
        $this->assertEquals($id, $found[0]->getId());
        $this->assertEquals($comment, $found[0]->getComment());
    }

    public function testSearchMethodSeveralOk()
    {
        $score   = 10;
        $comment = 'review number two';

        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        // create all
        $fixturesData = $this->searchReviewDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'hotel'   => $hotel,
                'score'   => $data[0],
                'comment' => $data[1],
            ]);
        }
        // plus duplicates
        //      by score:
        $this->repo->create([
            'hotel'   => $hotel,
            'score'   => $score,
            'comment' => 'review with same score',
        ]);
        //      by comment:
        $this->repo->create([
            'hotel'   => $hotel,
            'score'   => 4,
            'comment' => $comment,
        ]);

        //search existing by score
        $found = $this->repo->search(['score' => $score]);
        $this->assertNotEmpty($found);
        $this->assertCount(2, $found);

        //search existing by comment
        $found = $this->repo->search(['comment' => $comment]);
        $this->assertNotEmpty($found);
        $this->assertCount(2, $found);

        //search existing by hotel - all are same hotel reviews
        $found = $this->repo->search(['hotelId' => $hotel->getId()]);
        $this->assertNotEmpty($found);
        $this->assertCount(count($fixturesData) + 2, $found);
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function searchReviewDataProvider()
    {
        return [
            'review-1' => [
                0,
                'review #1',
            ],
            'review-2' => [
                10,
                'review number two',
            ],
            'review-3' => [
                3,
                'review ###3',
            ],
        ];
    }

    /**
     * @dataProvider overtimeDayDataProvider
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param int      $expectedCount
     *
     * @throws ORMException
     */
    public function testOvertimeDayOk(DateTime $startDate, DateTime $endDate, int $expectedCount)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        // create all
        $fixturesData = $this->overtimeReviewsDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'hotel'        => $hotel,
                'score'        => $data[0],
                'comment'      => $data[1],
                'created_date' => $data[2],
            ]);
        }

        //overtime by day
        $found = $this->repo->overtimeGroupDay($hotel->getId(), $startDate, $endDate);
        $this->assertNotEmpty($found);
        $this->assertCount($expectedCount, $found);
    }

    public function overtimeDayDataProvider()
    {
        $now = new DateTime();

        return [
            'current day'                 => [
                $now,
                $now,
                1
            ],
            '1 day range'                 => [
                (clone $now)->sub(new DateInterval('P1D')),
                $now,
                2
            ],
            'max day range(29), set more' => [
                (clone $now)->sub(new DateInterval('P40D')),
                $now,
                4
            ],
        ];
    }

    /**
     * @dataProvider overtimeWeekDataProvider
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param int      $expectedCount
     *
     * @throws ORMException
     */
    public function testOvertimeWeekOk(DateTime $startDate, DateTime $endDate, int $expectedCount)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        // create all
        $fixturesData = $this->overtimeReviewsDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'hotel'        => $hotel,
                'score'        => $data[0],
                'comment'      => $data[1],
                'created_date' => $data[2],
            ]);
        }

        //overtime by day
        $found = $this->repo->overtimeGroupWeek($hotel->getId(), $startDate, $endDate);
        $expectedCount
            ? $this->assertNotEmpty($found)
            : $this->assertEmpty($found);
        $this->assertCount($expectedCount, $found);
    }

    public function overtimeWeekDataProvider()
    {
        $now = new DateTime();

        return [
            'current day'                    => [
                $now,
                $now,
                0
            ],
            '1 day range'                    => [
                (clone $now)->sub(new DateInterval('P1D')),
                $now,
                0
            ],
            '40 days in range(30 - 89 days)' => [
                (clone $now)->sub(new DateInterval('P40D')),
                $now,
                1
            ],
            'max days range(30 - 89 days)'   => [
                (clone $now)->sub(new DateInterval('P100D')),
                $now,
                2
            ],
        ];
    }

    /**
     * @dataProvider overtimeMonthDataProvider
     *
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param int      $expectedCount
     *
     * @throws ORMException
     */
    public function testOvertimeMonthOk(DateTime $startDate, DateTime $endDate, int $expectedCount)
    {
        // new hotel
        $hotel = $this->hotelRepo->create([
            'name' => md5(random_bytes(120)),
        ]);

        // create all
        $fixturesData = $this->overtimeReviewsDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'hotel'        => $hotel,
                'score'        => $data[0],
                'comment'      => $data[1],
                'created_date' => $data[2],
            ]);
        }

        //overtime by day
        $found = $this->repo->overtimeGroupMonth($hotel->getId(), $startDate, $endDate);
        $expectedCount
            ? $this->assertNotEmpty($found)
            : $this->assertEmpty($found);
        $this->assertCount($expectedCount, $found);
    }

    public function overtimeMonthDataProvider()
    {
        $now = new DateTime();

        return [
            'current day'                => [
                $now,
                $now,
                0
            ],
            '1 day range'                => [
                (clone $now)->sub(new DateInterval('P1D')),
                $now,
                0
            ],
            '40 days in range(>89 days)' => [
                (clone $now)->sub(new DateInterval('P40D')),
                $now,
                0
            ],
            '90 days range(>89 days)'    => [
                (clone $now)->sub(new DateInterval('P100D')),
                $now,
                1
            ],
            'max range(>89 days)'        => [
                (clone $now)->sub(new DateInterval('P2Y')),
                $now,
                2
            ],
        ];
    }

    public function overtimeReviewsDataProvider()
    {
        $now = new DateTime();

        return [
            'review-1-now'         => [
                10,
                'review #1',
                $now,
            ],
            'review-2-yesterday-1' => [
                2,
                'review #2.1',
                (clone $now)->sub(new DateInterval('P1D')),
            ],
            'review-2-yesterday-2' => [
                2,
                'review #2.2',
                (clone $now)->sub(new DateInterval('P1D')),
            ],
            'review-3-p2d'         => [
                3,
                'review #3',
                (clone $now)->sub(new DateInterval('P2D')),
                $now,
            ],
            'review-4-p29d'        => [
                4,
                'review #4',
                (clone $now)->sub(new DateInterval('P29D')),
            ],
            'review-5-p30d'        => [
                5,
                'review #5',
                (clone $now)->sub(new DateInterval('P30D')),
            ],
            'review-6-p89d'        => [
                6,
                'review #6',
                (clone $now)->sub(new DateInterval('P89D')),
            ],
            'review-7-p90d'        => [
                7,
                'review #7',
                (clone $now)->sub(new DateInterval('P90D')),
            ],
            'review-8-p120d'       => [
                8,
                'review #8',
                (clone $now)->sub(new DateInterval('P120D')),
            ],
        ];
    }
}
