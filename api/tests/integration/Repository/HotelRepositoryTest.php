<?php

namespace Tests\Integration\Repository;

use App\Entity\Hotel;
use App\Repository\HotelRepository;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class HotelRepositoryTest
 *
 * @package Tests\Integration\Repository
 */
class HotelRepositoryTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var HotelRepository $repo
     */
    private $repo;

    protected function setUp(): void
    {
        $kernel     = self::bootKernel();
        $this->em   = $kernel->getContainer()
                             ->get('doctrine')
                             ->getManager();
        $this->repo = $this->em->getRepository(Hotel::class);

        $this->em->createQueryBuilder()
                 ->delete(Hotel::class, 'r')
                 ->getQuery()
                 ->execute();
    }

    /**
     * @dataProvider createHotelDataProvider
     *
     * @param $name
     *
     * @throws ORMException
     */
    public function testCreateMethodOk($name)
    {
        $obj = $this->repo->create([
            'name' => $name,
        ]);

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->getId());
        $this->assertInstanceOf(Hotel::class, $obj);
        $this->assertEquals($name, $obj->getName());
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function createHotelDataProvider()
    {
        return [
            'rand-name-1' => [
                md5(random_bytes(32)),
            ],
            'rand-name-2' => [
                md5(random_bytes(32)),
            ],
            'rand-name-3' => [
                md5(random_bytes(32)),
            ],
        ];
    }

    /**
     * @dataProvider createHotelFailDataProvider
     *
     * @param $attributes
     *
     * @throws ORMException
     */
    public function testCreateMethodFail($attributes)
    {
        $this->expectException(NotNullConstraintViolationException::class);

        $this->repo->create($attributes);
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function createHotelFailDataProvider()
    {
        return [
            'no-input'             => [null],
            'partly-missing: name' => [['other' => 123]],
        ];
    }


    /**
     * @dataProvider updateHotelDataProvider
     *
     * @param $name
     * @param $updateName
     *
     * @throws ORMException
     */
    public function testUpdateMethodOk($name, $updateName)
    {
        //create new
        $obj = $this->repo->create([
            'name' => $name,
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(Hotel::class, $obj);
        $this->assertEquals($name, $obj->getName());

        //update
        $obj = $this->repo->update(
            $obj,
            [
                'name' => $updateName,
            ]
        );

        $this->assertEquals($id, $obj->getId());
        $this->assertEquals($updateName, $obj->getName());
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function updateHotelDataProvider()
    {
        return [
            // name, update name
            'rand-name-1' => [
                md5(random_bytes(32)),
                md5(random_bytes(32)),
            ],
            'rand-name-2' => [
                md5(random_bytes(32)),
                md5(random_bytes(32)),
            ],
            'rand-name-3' => [
                md5(random_bytes(32)),
                md5(random_bytes(32)),
            ],
        ];
    }

    /**
     * @dataProvider createHotelFailDataProvider
     *
     * @param $attributes
     *
     * @throws ORMException
     */
    public function testUpdateMethodPartlyOk($attributes)
    {
        $obj = $this->repo->create([
            'name' => 'created record',
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($id);
        $this->assertInstanceOf(Hotel::class, $obj);

        $obj = $this->repo->update($obj, $attributes);

        $this->assertEquals($id, $obj->getId());
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $obj->getName());
        }
    }

    /**
     * @dataProvider createHotelDataProvider
     *
     * @param $name
     *
     * @throws ORMException
     */
    public function testDeleteMethodOk($name)
    {
        $obj = $this->repo->create([
            'name' => $name,
        ]);
        $id  = $obj->getId();

        $this->assertNotEmpty($obj);
        $this->assertNotEmpty($obj->getId());
        $this->assertInstanceOf(Hotel::class, $obj);

        $this->repo->delete($obj);

        $this->assertNull($this->repo->find($id));
    }

    /**
     * @throws ORMException
     */
    public function testDeleteMethodFail()
    {
        $this->expectException(ORMInvalidArgumentException::class);

        $this->repo->delete(new Hotel(['id' => -1]));
    }


    /**
     * @dataProvider searchHotelDataProvider
     *
     * @param $name
     *
     * @throws ORMException
     * @throws Exception
     */
    public function testSearchMethodOk($name)
    {
        // create all
        $fixturesData = $this->searchHotelDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'name' => $data[0],
            ]);
        }

        //search existing one
        $found = $this->repo->search(['name' => $name]);

        $this->assertNotEmpty($found);
        $this->assertCount(1, $found);
        $this->assertNotEmpty($found[0]);
        $this->assertNotEmpty($found[0]->getId());
        $this->assertInstanceOf(Hotel::class, $found[0]);
        $this->assertEquals($name, $found[0]->getName());
    }

    public function testSearchMethodSeveralOk()
    {
        $name = 'record number two';

        // create all
        $fixturesData = $this->searchHotelDataProvider();
        foreach ($fixturesData as $data) {
            $this->repo->create([
                'name' => $data[0],
            ]);
        }
        // plus duplicates
        //      by name:
        $this->repo->create([
            'name' => $name,
        ]);

        //search existing by name
        $found = $this->repo->search(['name' => $name]);
        $this->assertNotEmpty($found);
        $this->assertCount(2, $found);
    }

    /**
     * @return array[]
     * @throws Exception
     */
    public function searchHotelDataProvider()
    {
        return [
            'record-1' => [
                'record #1',
            ],
            'record-2' => [
                'record number two',
            ],
            'record-3' => [
                'record ###3',
            ],
        ];
    }
}
