<?php

namespace App\Abstracts;

use Exception;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RestControllerAbstract
 *
 * @package App\Abstracts
 */
abstract class RestControllerAbstract extends AbstractController
{
    protected const API_RESPONSE_CLASS = '';
    protected const CODE_OK            = '';
    protected const MSG_FOUND          = '';
    protected const CODE_CREATED       = '';
    protected const MSG_CREATED        = '';
    protected const CODE_NOT_FOUND     = '';
    protected const MSG_NOT_FOUND      = '';
    protected const CODE_BAD_REQUEST   = '';
    protected const MSG_BAD_REQUEST    = '';

    protected const REQUIRED_CONSTANTS = [
        'API_RESPONSE_CLASS',
        'CODE_OK',
        'MSG_FOUND',
        'CODE_CREATED',
        'MSG_CREATED',
        'CODE_NOT_FOUND',
        'MSG_NOT_FOUND',
        'CODE_BAD_REQUEST',
        'MSG_BAD_REQUEST',
    ];
    protected Serializer $serializer;


    /**
     * RestControllerAbstract constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        foreach (static::REQUIRED_CONSTANTS as $const) {
            if (empty(constant(static::class . '::' . $const))) {
                throw new Exception('constant is not defined ' . $const);
            }
        }

        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * General Ok data response
     *
     * @param object $data
     *
     * @return JsonResponse
     */
    protected function okJson(object $data): JsonResponse
    {
        $className   = static::API_RESPONSE_CLASS;
        $json        = new $className([
            'code'    => static::CODE_OK,
            'message' => static::MSG_FOUND,
            'data'    => $data,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
    }

    /**
     * General Created data response
     *
     * @param object $data
     *
     * @return JsonResponse
     */
    protected function createdJson(object $data): JsonResponse
    {
        $className   = static::API_RESPONSE_CLASS;
        $json        = new $className([
            'code'    => static::CODE_CREATED,
            'message' => static::MSG_CREATED,
            'data'    => $data,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_CREATED, [], true);
    }

    /**
     * General Not Found response
     *
     * @return JsonResponse
     */
    protected function notFoundJson(): JsonResponse
    {
        $className   = static::API_RESPONSE_CLASS;
        $json        = new $className([
            'code'    => static::CODE_NOT_FOUND,
            'message' => static::MSG_NOT_FOUND,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_NOT_FOUND, [], true);
    }

    /**
     * General Bad Request response
     *
     * @return JsonResponse
     */
    protected function badRequestJson(): JsonResponse
    {
        $className   = static::API_RESPONSE_CLASS;
        $json        = new $className([
            'code'    => static::CODE_BAD_REQUEST,
            'message' => static::MSG_BAD_REQUEST,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_BAD_REQUEST, [], true);
    }
}
