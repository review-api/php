<?php

namespace App\Abstracts;

/**
 * Class EntityAbstract
 *
 * @package App\Abstracts
 */
abstract class EntityAbstract
{
    /**
     * Constructor
     *
     * @param array|null $attributes
     */
    public function __construct(array $attributes = null)
    {
        if (!empty($attributes)) {
            foreach ($attributes as $k => $v) {
                if (property_exists(static::class, $k) || property_exists(static::class, static::propertyName($k))) {
                    $this->{static::setterByName($k)}($v);
                }
            }
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Helper method to convert property name into setter name
     *
     * @param string $name
     *
     * @return string
     */
    public static function setterByName(string $name): string
    {
        return 'set' . static::nameToCamelCase($name);
    }

    /**
     * Helper method to convert provided name to property camel-case name
     *
     * @param string $name
     *
     * @return string
     */
    public static function propertyName(string $name): string
    {
        return lcfirst(static::nameToCamelCase($name));
    }

    /**
     * @param string $name
     *
     * @return string
     */
    protected static function nameToCamelCase(string $name): string
    {
        return str_replace('_', '', ucwords($name, '_'));
    }
}
