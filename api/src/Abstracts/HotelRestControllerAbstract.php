<?php

namespace App\Abstracts;

use Swagger\Server\Model\HotelApiResponse;

/**
 * Class HotelRestControllerAbstract
 *
 * @package App\Abstracts
 */
abstract class HotelRestControllerAbstract extends RestControllerAbstract
{
    protected const API_RESPONSE_CLASS = HotelApiResponse::class;
}
