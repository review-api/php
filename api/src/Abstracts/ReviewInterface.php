<?php

namespace App\Abstracts;

/**
 * @inheritdoc
 */
interface ReviewInterface
{
    public function getHotelId();

    public function setHotelId($hotelId);

    public function getScore();

    public function setScore($score);

    public function getComment();

    public function setComment($comment = null);
}
