<?php

namespace App\Abstracts;

use Swagger\Server\Model\ReviewApiResponse;

/**
 * Class ReviewRestControllerAbstract
 *
 * @package App\Abstracts
 */
abstract class ReviewRestControllerAbstract extends RestControllerAbstract
{
    protected const API_RESPONSE_CLASS = ReviewApiResponse::class;
}
