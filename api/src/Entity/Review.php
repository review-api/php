<?php

namespace App\Entity;

use App\Abstracts\EntityAbstract;
use App\Repository\ReviewRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review", indexes={@ORM\Index(name="review_hotel_id_fk", columns={"hotel_id"})})
 * @ORM\Entity(repositoryClass=ReviewRepository::class)
 */
class Review extends EntityAbstract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer", nullable=false)
     */
    private $score;

    /**
     * @var int
     *
     * @ORM\Column(name="hotel_id", type="integer", nullable=false)
     */
    private $hotelId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=256, nullable=false)
     */
    private $comment;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private DateTime $createdDate;

    /**
     * @var Hotel
     *
     * @ORM\ManyToOne(targetEntity="Hotel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hotel_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private Hotel $hotel;

    /**
     * Review constructor.
     * @inheritDoc
     */
    public function __construct(array $attributes = null)
    {
        parent::__construct($attributes);
        if (empty($this->id) && empty($this->createdDate)) {
            $this->createdDate = new DateTime();
        }
    }

    /**
     * Gets hotelId.
     *
     * @return int
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * Sets hotelId.
     *
     * @param int $hotelId
     *
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = $hotelId;

        return $this;
    }

    /**
     * Gets score.
     *
     * @return int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Sets score.
     *
     * @param int $score
     *
     * @return $this
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Gets comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Sets comment.
     *
     * @param string|null $comment
     *
     * @return $this
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Gets createdDate.
     *
     * @return DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Sets createdDate.
     *
     * @param DateTime $createdDate
     *
     * @return $this
     */
    public function setCreatedDate(DateTime $createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Gets Hotel.
     *
     * @return Hotel
     */
    public function getHotel(): Hotel
    {
        return $this->hotel;
    }

    /**
     * Sets Hotel.
     *
     * @param Hotel $hotel
     *
     * @return $this
     */
    public function setHotel(Hotel $hotel)
    {
        $this->hotel = $hotel;

        return $this;
    }

}
