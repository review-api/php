<?php

namespace App\Controller;

use App\Abstracts\ReviewInterface;
use App\Abstracts\ReviewRestControllerAbstract;
use App\Entity\Hotel;
use App\Entity\Review;
use App\Repository\ReviewRepository;
use Swagger\Server\Model\DTOOvertime;
use Swagger\Server\Model\DTOOvertimeGroups;
use Swagger\Server\Model\DTOSearchReview;
use Swagger\Server\Model\OvertimeApiResponse;
use Swagger\Server\Model\ReviewApiResponse;
use Swagger\Server\Model\DTOReview;
use Swagger\Server\Model\SearchReviewApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;
use Throwable;

/**
 * Class ReviewController
 *
 * @package App\Controller
 */
class ReviewController extends ReviewRestControllerAbstract
{
    const CODE_OK          = 2000200;
    const CODE_CREATED     = 2000201;
    const CODE_NOT_FOUND   = 2000404;
    const CODE_BAD_REQUEST = 2000400;
    const MSG_FOUND        = 'review found';
    const MSG_FOUND_MANY   = 'reviews found';
    const MSG_CREATED      = 'review created';
    const MSG_NOT_FOUND    = 'review not found';
    const MSG_BAD_REQUEST  = 'bad request';

    const VALIDATE_SCORE_MIN = 0;
    const VALIDATE_SCORE_MAX = 10;

    /**
     * @OA\Get(
     *   tags={"Review"},
     *   path="/review/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\Response(
     *       response="default",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/ReviewApiResponse")
     *   )
     * )
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function getAction(string $id)
    {
        if (empty($id) || !is_numeric($id)) {
            return $this->badRequestJson();
        }

        /** @var Review|false $obj */
        $obj = $this->getDoctrine()->getRepository(Review::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTOReview([
            'id'           => $id,
            'hotel_id'     => $obj->getHotelId(),
            'score'        => $obj->getScore(),
            'comment'      => $obj->getComment(),
            'created_date' => $obj->getCreatedDate(),
        ]));
    }

    /**
     * @OA\Post(
     *   tags={"Review"},
     *   path="/review",
     *   @OA\RequestBody(
     *     required=true,
     *     description="Review data to create",
     *     @OA\JsonContent(ref="#/components/schemas/DTOReview")
     *   ),
     *   @OA\Response(
     *       response=201,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/ReviewApiResponse")
     *   )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }

        try {
            /** @var DTOReview $json */
            $json = $this->serializer->deserialize($jsonContent, DTOReview::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        if (!$this->validateScore($json)) {
            return $this->badRequestJson();
        }
        $attributes = $json->toArray();
        // excluding from input
        unset($attributes['created_date']);

        if (!empty($json->getHotelId()) && is_numeric($json->getHotelId())) {
            $hotel               = $this->getDoctrine()->getRepository(Hotel::class)->find($json->getHotelId());
            $attributes['hotel'] = $hotel;
        }

        if (empty($attributes['hotel'])) {
            return $this->badRequestJson();
        }

        /** @var Review|false $obj */
        $obj = $this->getDoctrine()->getRepository(Review::class)->create($attributes);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->createdJson(new DTOReview([
            'id'           => $obj->getId(),
            'hotel_id'     => $obj->getHotelId(),
            'score'        => $obj->getScore(),
            'comment'      => $obj->getComment(),
            'created_date' => $obj->getCreatedDate(),
        ]));
    }

    /**
     * @OA\Patch(
     *   tags={"Review"},
     *   path="/review/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\RequestBody(
     *     required=true,
     *     description="Review data for partial update",
     *     @OA\JsonContent(ref="#/components/schemas/DTOReview")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/ReviewApiResponse")
     *   )
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function patchAction(Request $request, string $id)
    {
//        $jsonContent = Request::createFromGlobals()->getContent();
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOReview $json */
            $json = $this->serializer->deserialize($jsonContent, DTOReview::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        if (!empty($json->getScore()) && !$this->validateScore($json)) {
            return $this->badRequestJson();
        }

        $attributes = array_filter($json->toArray(), fn($v) => !empty($v));
        // excluding from input
        unset($attributes['created_date']);

        /** @var Review|false $obj */
        $obj = $this->getDoctrine()->getRepository(Review::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Review::class)->update(
            $obj,
            $attributes
        );
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTOReview([
            'id'           => $obj->getId(),
            'hotel_id'     => $obj->getHotelId(),
            'score'        => $obj->getScore(),
            'comment'      => $obj->getComment(),
            'created_date' => $obj->getCreatedDate(),
        ]));
    }

    /**
     * @OA\Put(
     *   tags={"Review"},
     *   path="/review/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\RequestBody(
     *     required=true,
     *     description="Review data for full update",
     *     @OA\JsonContent(ref="#/components/schemas/DTOReview")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/ReviewApiResponse")
     *   )
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function putAction(Request $request, string $id)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOReview $json */
            $json = $this->serializer->deserialize($jsonContent, DTOReview::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        if (!$this->validateScore($json)) {
            return $this->badRequestJson();
        }

        $json->setId($id);

        /** @var Review|false $obj */
        $obj = $this->getDoctrine()->getRepository(Review::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $attributes = $json->toArray();
        // excluding from input
        unset($attributes['created_date']);

        $this->getDoctrine()->getRepository(Review::class)->update(
            $obj,
            $attributes
        );
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTOReview([
            'id'           => $obj->getId(),
            'hotel_id'     => $obj->getHotelId(),
            'score'        => $obj->getScore(),
            'comment'      => $obj->getComment(),
            'created_date' => $obj->getCreatedDate(),
        ]));
    }

    /**
     * @OA\Delete(
     *   tags={"Review"},
     *   path="/review/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\Response(
     *       response="default",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/ReviewApiResponse")
     *   )
     * )
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function deleteAction(string $id)
    {
        if (empty($id) || !is_numeric($id)) {
            return $this->badRequestJson();
        }

        /** @var Review|false $obj */
        $obj = $this->getDoctrine()->getRepository(Review::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Review::class)->delete($obj);

        return $this->okJson(new DTOReview([
            'id'           => $obj->getId(),
            'hotel_id'     => $obj->getHotelId(),
            'score'        => $obj->getScore(),
            'comment'      => $obj->getComment(),
            'created_date' => $obj->getCreatedDate(),
        ]));
    }

    /**
     * @OA\Post(
     *   tags={"Review"},
     *   path="/review/search",
     *   @OA\RequestBody(
     *     required=true,
     *     description="Review data to search",
     *     @OA\JsonContent(ref="#/components/schemas/DTOSearchReview")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/SearchReviewApiResponse")
     *   )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchAction(Request $request)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOReview $json */
            $json = $this->serializer->deserialize($jsonContent, DTOSearchReview::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        if (!$this->validateScore($json)) {
            return $this->badRequestJson();
        }

        $criteria = [];
        if (!empty($json->getHotelId())) {
            $criteria['hotelId'] = $json->getHotelId();
        }
        if (!empty($json->getScore())) {
            $criteria['score'] = $json->getScore();
        }
        if (!empty($json->getComment())) {
            $criteria['comment'] = $json->getComment();
        }

        if (empty($criteria)) {
            return $this->notFoundJson();
        }

        /** @var Review[]|false $obj */
        $found = $this->getDoctrine()->getRepository(Review::class)->search($criteria);

        if (empty($found)) {
            return $this->notFoundJson();
        }

        return $this->searchJson($found);
    }

    /**
     * @OA\Get(
     *   tags={"Overtime"},
     *   path="/overtime/{hotel_id}/{date_start}/{date_end}",
     *   @OA\Parameter(
     *     name="hotel_id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="Hotel ID value"
     *   ),
     *   @OA\Parameter(
     *     name="date_start",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string", format="date"),
     *     description="Date start"
     *   ),
     *   @OA\Parameter(
     *     name="date_end",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string", format="date"),
     *     description="Date end"
     *   ),
     *   @OA\Response(
     *       response="200",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/OvertimeApiResponse")
     *   )
     * )
     *
     * @param string $hotel_id
     * @param string $date_start
     * @param string $date_end
     *
     * @return JsonResponse
     */
    public function overtimeAction(string $hotel_id, string $date_start, string $date_end)
    {
        if (empty($hotel_id) || !is_numeric($hotel_id)) {
            return $this->badRequestJson();
        }

        $date_start = date_create_from_format('Y-m-d', $date_start);
        if (empty($date_start)) {
            return $this->badRequestJson();
        }
        $date_end = date_create_from_format('Y-m-d', $date_end);
        if (empty($date_end)) {
            return $this->badRequestJson();
        }

        /** @var array $data */
        $data = $this->getDoctrine()->getRepository(Review::class)->overtime($hotel_id, $date_start, $date_end);
        if (empty($data)) {
            return $this->notFoundJson();
        }

        return $this->overtimeJson($data);
    }

    /**
     * General Search data response
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    protected function searchJson(array $data): JsonResponse
    {
        $dtoData = [];
        /** @var Review $obj */
        foreach ($data as $obj) {
            $dtoData[] = new DTOReview([
                'id'           => $obj->getId(),
                'hotel_id'     => $obj->getHotelId(),
                'score'        => $obj->getScore(),
                'comment'      => $obj->getComment(),
                'created_date' => $obj->getCreatedDate(),
            ]);
        }

        $json        = new SearchReviewApiResponse([
            'code'    => static::CODE_OK,
            'message' => static::MSG_FOUND_MANY,
            'data'    => $dtoData,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
    }

    /**
     * Overtime data response
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    protected function overtimeJson(array $data): JsonResponse
    {
        $fields  = [
            ReviewRepository::DATE_GROUP_DAY,
            ReviewRepository::DATE_GROUP_WEEK,
            ReviewRepository::DATE_GROUP_MONTH
        ];
        $dtoData = array_fill_keys($fields, []);

        foreach ($fields as $field) {
            foreach ($data[$field] as $row) {
                $dtoData[$field][] = new DTOOvertime($row);
            }
        }

        $json        = new OvertimeApiResponse([
            'code'    => static::CODE_OK,
            'message' => static::MSG_FOUND_MANY,
            'data'    => new DTOOvertimeGroups($dtoData),
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
    }

    protected function validateScore(ReviewInterface $obj): bool
    {
        return $obj->getScore() >= static::VALIDATE_SCORE_MIN && $obj->getScore() <= static::VALIDATE_SCORE_MAX;
    }
}
