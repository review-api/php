<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @OA\Info(
     *   title="Review Rest API",
     *   version="1.0.0",
     *   description="Consumes a hotel-id and a date range from http requests and returns the overtime average score of the hotel for grouped date ranges. The date range is grouped as follows:\n
    - 1 - 29 days: Grouped daily\n
    - 30 - 89 days: Grouped weekly\n
    - More than 89 days: Grouped monthly",
     *   @OA\Contact(
     *     name="Alex",
     *     email="alex@webz.asia"
     *   ),
     *   @OA\License(
     *     name="Apache 2.0",
     *     url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *   )
     * )
     *
     * @Route("/", name="default_index")
     *
     * @return JsonResponse
     */
    public function index()
    {
        return new JsonResponse();
    }
}
