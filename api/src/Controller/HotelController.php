<?php

namespace App\Controller;

use App\Abstracts\HotelRestControllerAbstract;
use App\Entity\Hotel;
use Swagger\Server\Model\DTOSearchHotel;
use Swagger\Server\Model\HotelApiResponse;
use Swagger\Server\Model\DTOHotel;
use Swagger\Server\Model\SearchHotelApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use OpenApi\Annotations as OA;
use Throwable;

/**
 * Class HotelController
 *
 * @package App\Controller
 */
class HotelController extends HotelRestControllerAbstract
{
    const CODE_OK          = 1000200;
    const CODE_CREATED     = 1000201;
    const CODE_NOT_FOUND   = 1000404;
    const CODE_BAD_REQUEST = 1000400;
    const MSG_FOUND        = 'hotel found';
    const MSG_FOUND_MANY   = 'hotels found';
    const MSG_CREATED      = 'hotel created';
    const MSG_NOT_FOUND    = 'hotel not found';
    const MSG_BAD_REQUEST  = 'bad request';

    /**
     * @OA\Get(
     *   tags={"Hotel"},
     *   path="/hotel/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\Response(
     *       response="default",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/HotelApiResponse")
     *   )
     * )
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function getAction(string $id)
    {
        if (empty($id) || !is_numeric($id)) {
            return $this->badRequestJson();
        }

        /** @var Hotel|false $obj */
        $obj = $this->getDoctrine()->getRepository(Hotel::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTOHotel([
            'id'   => $id,
            'name' => $obj->getName(),
        ]));
    }

    /**
     * @OA\Post(
     *   tags={"Hotel"},
     *   path="/hotel",
     *   @OA\RequestBody(
     *     required=true,
     *     description="Hotel data to create",
     *     @OA\JsonContent(ref="#/components/schemas/DTOHotel")
     *   ),
     *   @OA\Response(
     *       response=201,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/HotelApiResponse")
     *   )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOHotel $json */
            $json = $this->serializer->deserialize($jsonContent, DTOHotel::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        /** @var Hotel|false $obj */
        $obj = $this->getDoctrine()->getRepository(Hotel::class)->create($json->toArray());
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->createdJson(new DTOHotel([
            'id'   => $obj->getId(),
            'name' => $obj->getName(),
        ]));
    }

    /**
     * @OA\Patch(
     *   tags={"Hotel"},
     *   path="/hotel/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\RequestBody(
     *     required=true,
     *     description="Hotel data for partial update",
     *     @OA\JsonContent(ref="#/components/schemas/DTOHotel")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/HotelApiResponse")
     *   )
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function patchAction(Request $request, string $id)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOHotel $json */
            $json = $this->serializer->deserialize($jsonContent, DTOHotel::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }
        $attributes = array_filter($json->toArray(), fn($v) => !empty($v));

        /** @var Hotel|false $obj */
        $obj = $this->getDoctrine()->getRepository(Hotel::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Hotel::class)->update(
            $obj,
            $attributes
        );
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTOHotel([
            'id'   => $obj->getId(),
            'name' => $obj->getName(),
        ]));
    }

    /**
     * @OA\Put(
     *   tags={"Hotel"},
     *   path="/hotel/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\RequestBody(
     *     required=true,
     *     description="Hotel data for full update",
     *     @OA\JsonContent(ref="#/components/schemas/DTOHotel")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/HotelApiResponse")
     *   )
     * )
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function putAction(Request $request, string $id)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOHotel $json */
            $json = $this->serializer->deserialize($jsonContent, DTOHotel::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }
        $json->setId($id);

        /** @var Hotel|false $obj */
        $obj = $this->getDoctrine()->getRepository(Hotel::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Hotel::class)->update(
            $obj,
            $json->toArray()
        );
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        return $this->okJson(new DTOHotel([
            'id'   => $obj->getId(),
            'name' => $obj->getName(),
        ]));
    }

    /**
     * @OA\Delete(
     *   tags={"Hotel"},
     *   path="/hotel/{id}",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="number"),
     *     description="ID value"
     *   ),
     *   @OA\Response(
     *       response="default",
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/HotelApiResponse")
     *   )
     * )
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function deleteAction(string $id)
    {
        if (empty($id) || !is_numeric($id)) {
            return $this->badRequestJson();
        }

        /** @var Hotel|false $obj */
        $obj = $this->getDoctrine()->getRepository(Hotel::class)->find($id);
        if (empty($obj)) {
            return $this->notFoundJson();
        }

        $this->getDoctrine()->getRepository(Hotel::class)->delete($obj);

        return $this->okJson(new DTOHotel([
            'id'   => $obj->getId(),
            'name' => $obj->getName(),
        ]));
    }

    /**
     * @OA\Post(
     *   tags={"Hotel"},
     *   path="/hotel/search",
     *   @OA\RequestBody(
     *     required=true,
     *     description="Hotel data to search",
     *     @OA\JsonContent(ref="#/components/schemas/DTOSearchHotel")
     *   ),
     *   @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(ref="#/components/schemas/SearchHotelApiResponse")
     *   )
     * )
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchAction(Request $request)
    {
        $jsonContent = $request->getContent();

        if (empty($jsonContent)) {
            return $this->badRequestJson();
        }
        try {
            /** @var DTOHotel $json */
            $json = $this->serializer->deserialize($jsonContent, DTOSearchHotel::class, 'json');
        } catch (Throwable $e) {
            return $this->badRequestJson();
        }

        $criteria = [];
        if (!empty($json->getName())) {
            $criteria['name'] = $json->getName();
        }

        if (empty($criteria)) {
            return $this->notFoundJson();
        }

        /** @var Hotel[]|false $obj */
        $found = $this->getDoctrine()->getRepository(Hotel::class)->search($criteria);

        if (empty($found)) {
            return $this->notFoundJson();
        }

        return $this->searchJson($found);
    }

    /**
     * General Search data response
     *
     * @param array $data
     *
     * @return JsonResponse
     */
    protected function searchJson(array $data): JsonResponse
    {
        $dtoData = [];
        /** @var Hotel $obj */
        foreach ($data as $obj) {
            $dtoData[] = new DTOHotel([
                'id'   => $obj->getId(),
                'name' => $obj->getName(),
            ]);
        }

        $json        = new SearchHotelApiResponse([
            'code'    => static::CODE_OK,
            'message' => static::MSG_FOUND_MANY,
            'data'    => $dtoData,
        ]);
        $jsonContent = $this->serializer->serialize($json, 'json');

        return new JsonResponse($jsonContent, Response::HTTP_OK, [], true);
    }
}
