<?php

namespace App\Command;

use App\Entity\Hotel;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\Company;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class DoctrineFixturesCreateHotelReviewsCommand
 *
 * @package App\Command
 */
class DoctrineFixturesCreateHotelReviewsCommand extends Command
{
    const TOTAL_HOTELS       = 10;
    const TOTAL_1K_REVIEWS   = 100;
    const INSERT_REVIEW_HEAD = 'INSERT INTO review(hotel_id, score, comment, created_date) VALUES ';

    protected static $defaultName = 'doctrine:fixtures:create-hotel-reviews';

    private EntityManagerInterface $manager;
    private Generator              $faker;
    private SymfonyStyle           $io;

    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct();
        $this->manager = $manager;
        $this->faker   = Factory::create('en_GB');
    }

    protected function configure()
    {
        $this
            ->setDescription('Generating all fixture hotels-reviews data')
            ->addOption('dry', null, InputOption::VALUE_NONE, 'Dry-run, no data to DB');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $dry      = false;

        if ($input->getOption('dry')) {
            $dry = true;
        }

        $this->createHotels();
        $allHotels = $this->manager->getRepository(Hotel::class)->findAll();
        for ($i = 0; $i < static::TOTAL_1K_REVIEWS; $i++) {
            $sql = $this->generate1kReviewInserts($allHotels, $dry);
            $this->io->note($i * 1000);
            if (!$dry) {
                $this->manager->getConnection()->prepare($sql)->execute();
            } else {
                $this->io->info($sql);
            }
        }

        $this->io->success('All data was generated');
        if (!$dry) {
            $this->io->success('And applied to DB');
        }

        return 0;
    }

    /**
     * Create all hotels
     */
    protected function createHotels()
    {
        $faker = new Company($this->faker);
        for ($i = 0; $i < static::TOTAL_HOTELS; $i++) {
            $this->manager->getRepository(Hotel::class)->create([
                'name' => $faker->company(),
            ]);
        }
        $this->manager->flush();
    }

    /**
     * Generate SQL Insert and parameters for execution
     *
     * @param array $allHotels
     * @param bool  $dry
     *
     * @return string
     */
    protected function generate1kReviewInserts(array $allHotels, bool $dry = false): string
    {
        $sql         = static::INSERT_REVIEW_HEAD;
        $parameters  = [];
        $sqlArr      = [];
        $totalHotels = count($allHotels);

        for ($i = 0; $i < 1000; $i++) {
            $hotel       = $allHotels[$this->faker->numberBetween(0, $totalHotels - 1)];
            $createdDate = $this->faker->dateTimeBetween('-2 years', 'now', 'UTC');

            $parameters[] = [
                'hotel_id'    => $hotel->getId(),
                'score'       => $this->faker->numberBetween(0, 10),
                'comment'     => $this->faker->text(250),
                'createdDate' => $createdDate->format('Y-m-d H:i:s'),
            ];

            array_walk($parameters[count($parameters) - 1], fn($item) => str_replace("'", "\'", $item));
            $sqlArr[] = "('" . join("', '", $parameters[count($parameters) - 1]) . "')";
        }

        $sql .= join(',', $sqlArr) . ';';

        return $sql;
    }
}
