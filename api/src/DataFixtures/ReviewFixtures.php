<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use App\Repository\HotelRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Psr\Log\LoggerInterface;

/**
 * Class ReviewFixtures
 *
 * @package App\DataFixtures
 */
class ReviewFixtures extends Fixture
{
    public const TOTAL = 100000;

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('en_GB');

        /** @var HotelRepository $hotelRepo */
        $hotelRepo = $manager->getRepository(Hotel::class);
        /** @var []Hotel $allHotels */
        $allHotels   = $hotelRepo->findAll();
        $totalHotels = count($allHotels);

        for ($i = 0; $i < self::TOTAL; $i++) {
            $this->logger->notice($i);

            $hotel       = $allHotels[$faker->numberBetween(0, $totalHotels - 1)];
            $createdDate = $faker->dateTimeBetween('-2 years', 'now', 'UTC');

            $manager->getRepository(Review::class)->create([
                'hotel'       => $hotel,
                'score'       => $faker->numberBetween(0, 10),
                'comment'     => $faker->text(250),
                'createdDate' => $createdDate,
            ]);
            if ($i % 100 == 0) {
                $manager->flush();
            }
        }
    }
}
