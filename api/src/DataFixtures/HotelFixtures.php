<?php

namespace App\DataFixtures;

use App\Entity\Hotel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\Company;

/**
 * Class HotelFixtures
 *
 * @package App\DataFixtures
 */
class HotelFixtures extends Fixture
{
    public const TOTAL = 10;

    public function load(ObjectManager $manager)
    {
        $faker = new Company(Factory::create('en_GB'));
        for ($i = 0; $i < self::TOTAL; $i++) {
            $manager->getRepository(Hotel::class)->create([
                'name' => $faker->company(),
            ]);
        }
        $manager->flush();
    }
}
