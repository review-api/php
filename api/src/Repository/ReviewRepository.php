<?php

namespace App\Repository;

use App\Entity\Review;
use DateInterval;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class ReviewRepository
 *
 * @package App\Repository
 */
class ReviewRepository extends ServiceEntityRepository
{
    const DATE_GROUP_DAY   = 'day';
    const DATE_GROUP_WEEK  = 'calendarWeek';
    const DATE_GROUP_MONTH = 'month';

    /**
     * ReviewRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Review::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return Review
     * @throws ORMException
     */
    public function create(array $attributes = null): Review
    {
        $object = new Review($attributes);
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();

        return $object;
    }

    /**
     * @param Review     $object
     * @param array|null $attributes
     *
     * @return Review
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Review $object, array $attributes = null): Review
    {
        if (empty($attributes)) {
            return $object;
        }

        foreach ($attributes as $k => $v) {
            $method = Review::setterByName($k);
            if (method_exists($object, $method)) {
                $object->{$method}($v);
            }
        }
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();

        return $object;
    }

    /**
     * @param Review $object
     *
     * @throws ORMException
     */
    public function delete(Review $object)
    {
        $this->getEntityManager()->remove($object);
        $this->getEntityManager()->flush();
    }

    /**
     * @param array $criteria
     *
     * @return array|Review[]
     * @todo To write alternative method with Query Builder
     */
    public function search(array $criteria): array
    {
        return $this->findBy($criteria);
    }

    /**
     * @param string   $hotelId
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return array[]
     */
    public function overtime(string $hotelId, DateTime $dateStart, DateTime $dateEnd): array
    {
        return [
            self::DATE_GROUP_DAY   => $this->overtimeGroupDay($hotelId, $dateStart, $dateEnd),
            self::DATE_GROUP_WEEK  => $this->overtimeGroupWeek($hotelId, $dateStart, $dateEnd),
            self::DATE_GROUP_MONTH => $this->overtimeGroupMonth($hotelId, $dateStart, $dateEnd),
        ];
    }

    /**
     * @param string   $hotelId
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return array
     */
    public function overtimeGroupDay(string $hotelId, DateTime $dateStart, DateTime $dateEnd): array
    {
        $sqlDateRight = new DateTime();
        if ($sqlDateRight > $dateEnd) {
            $sqlDateRight = $dateEnd;
        }

        $sqlDateLeft = (clone $sqlDateRight)->sub(new DateInterval('P29D'));
        if ($sqlDateLeft < $dateStart) {
            $sqlDateLeft = $dateStart;
        }

        $qb = $this->createQueryBuilder('t')
                   ->select('t.hotelId, \'' . self::DATE_GROUP_DAY . '\' as dateGroup, DATE(t.createdDate) as grouped, COUNT(t.id) as reviewCount, AVG(t.score) as averageScore')
                   ->where('t.hotelId = :hotelId')
                   ->andWhere('DATE(t.createdDate) >= :dateStart')
                   ->andWhere('DATE(t.createdDate) <= :dateEnd')
                   ->groupBy('grouped')
                   ->orderBy('grouped', 'asc')
                   ->setParameters([
                       'hotelId'   => $hotelId,
                       'dateStart' => $sqlDateLeft->format('Y-m-d'),
                       'dateEnd'   => $sqlDateRight->format('Y-m-d'),
                   ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string   $hotelId
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return array
     */
    public function overtimeGroupWeek(string $hotelId, DateTime $dateStart, DateTime $dateEnd): array
    {
        $sqlDateRightOrig = new DateTime();
        if ($sqlDateRightOrig < $dateEnd) {
            $sqlDateRightOrig = $dateEnd;
        }
        $sqlDateRight = (clone $sqlDateRightOrig)->sub(new DateInterval('P30D'));

        $sqlDateLeft = (clone $sqlDateRightOrig)->sub(new DateInterval('P89D'));
        if ($sqlDateLeft < $dateStart) {
            $sqlDateLeft = $dateStart;
        }

        $qb = $this->createQueryBuilder('t')
                   ->select('t.hotelId, \'' . self::DATE_GROUP_WEEK . '\' as dateGroup, WEEK(t.createdDate) as grouped, COUNT(t.id) as reviewCount, AVG(t.score) as averageScore')
                   ->where('t.hotelId = :hotelId')
                   ->andWhere('DATE(t.createdDate) >= :dateStart')
                   ->andWhere('DATE(t.createdDate) <= :dateEnd')
                   ->groupBy('grouped')
                   ->orderBy('grouped', 'asc')
                   ->setParameters([
                       'hotelId'   => $hotelId,
                       'dateStart' => $sqlDateLeft->format('Y-m-d'),
                       'dateEnd'   => $sqlDateRight->format('Y-m-d'),
                   ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string   $hotelId
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     *
     * @return array
     */
    public function overtimeGroupMonth(string $hotelId, DateTime $dateStart, DateTime $dateEnd): array
    {
        $sqlDateRightOrig = new DateTime();
        if ($sqlDateRightOrig > $dateEnd) {
            $sqlDateRightOrig = $dateEnd;
        }
        $sqlDateRight = (clone $sqlDateRightOrig)->sub(new DateInterval('P90D'));

        $qb = $this->createQueryBuilder('t')
                   ->select('t.hotelId, \'' . self::DATE_GROUP_MONTH . '\' as dateGroup, DATE_FORMAT(t.createdDate, \'%Y-%m\') as grouped, COUNT(t.id) as reviewCount, AVG(t.score) as averageScore')
                   ->where('t.hotelId = :hotelId')
                   ->andWhere('DATE(t.createdDate) >= :dateStart')
                   ->andWhere('DATE(t.createdDate) <= :dateEnd')
                   ->groupBy('grouped')
                   ->orderBy('grouped', 'asc')
                   ->setParameters([
                       'hotelId'   => $hotelId,
                       'dateStart' => $dateStart->format('Y-m-d'),
                       'dateEnd'   => $sqlDateRight->format('Y-m-d'),
                   ]);

        return $qb->getQuery()->getResult();
    }
}
