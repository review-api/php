<?php

namespace App\Repository;

use App\Entity\Hotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class HotelRepository
 *
 * @package App\Repository
 */
class HotelRepository extends ServiceEntityRepository
{
    /**
     * HotelRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hotel::class);
    }

    /**
     * @param array|null $attributes
     *
     * @return Hotel
     * @throws ORMException
     */
    public function create(array $attributes = null): Hotel
    {
        $object = new Hotel($attributes);
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();

        return $object;
    }

    /**
     * @param Hotel     $object
     * @param array|null $attributes
     *
     * @return Hotel
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function update(Hotel $object, array $attributes = null): Hotel
    {
        if (empty($attributes)) {
            return $object;
        }

        foreach ($attributes as $k => $v) {
            $method = Hotel::setterByName($k);
            if (method_exists($object, $method)) {
                $object->{$method}($v);
            }
        }
        $this->getEntityManager()->persist($object);
        $this->getEntityManager()->flush();

        return $object;
    }

    /**
     * @param Hotel $object
     *
     * @throws ORMException
     */
    public function delete(Hotel $object)
    {
        $this->getEntityManager()->remove($object);
        $this->getEntityManager()->flush();
    }

    /**
     * @param array $criteria
     *
     * @return array|Hotel[]
     * @todo To write alternative method with Query Builder
     */
    public function search(array $criteria): array
    {
        return $this->findBy($criteria);
    }
}
