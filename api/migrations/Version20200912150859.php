<?php

declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200912150859 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Creating tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
        create table hotel
        (
            id   int auto_increment,
            name varchar(256) not null,
            constraint hotel_pk
                primary key (id)
        );
        
        create table review
        (
            id           int auto_increment,
            hotel_id     int          not null,
            score        int          not null,
            comment      varchar(256) not null,
            created_date timestamp    not null default current_timestamp,
            constraint review_pk
                primary key (id),
            constraint review_hotel_id_fk
                foreign key (hotel_id) references hotel (id) on delete cascade on update no action
        );
        SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql("drop table if exists hotel, review cascade");
    }
}
