# Coding challenge - Statistics APIs
## Intro
At the beginning idea was to reuse some parts(DTO Models) generated code with designed first [swagger.yml]() located in project root. But default swagger generator results for Symfony look quite outdated.

## Installation/setup
### Git
Clone the repo
```
git clone https://gitlab.com/review-api/php.git ./review-api
```

### Docker
All configuration is provided with `docker-compose.yml` and `./docker` folder for building **nginx** and **php-fpm** containers.

### Configuration
**Note:** There are two `.env` files required:
* In project root, used for docker configuration. There is `.env.dist` in repo as a template/list of expected vars. My recommendation/sample with values:
```dotenv
MYSQL_PASSWORD=123
MYSQL_ROOT_PASSWORD=12345
MYSQL_DATABASE=reviewapi
MYSQL_USER=user
TZ=UTC
``` 
* In Symfony app root `./api`, used for Symfony configuration. There is `.env.dist` in repo as a template/list of expected vars. My recommendation/sample with values:
```dotenv
# This file is a "template" of which env vars need to be defined for your application
# Copy this file to .env file for development, create environment variables when deploying to production
# https://symfony.com/doc/current/best_practices/configuration.html#infrastructure-related-configuration

###> symfony/framework-bundle ###
APP_ENV=dev
APP_SECRET=oka2a9cxhkwuwhqifpm2maqez5v8c629
#TRUSTED_PROXIES=127.0.0.1,127.0.0.2
#TRUSTED_HOSTS=localhost,example.com
###< symfony/framework-bundle ###

###> lexik/jwt-authentication-bundle ###
# Key paths should be relative to the project directory
JWT_PRIVATE_KEY_PATH=config/jwt/private.pem
JWT_PUBLIC_KEY_PATH=config/jwt/public.pem
JWT_SECRET_KEY=config/jwt/private.pem
JWT_PUBLIC_KEY=config/jwt/public.pem
JWT_PASSPHRASE=55m25w8wmmimekfapjohyfrn7t9dc4yu
###< lexik/jwt-authentication-bundle ###

###> doctrine/doctrine-bundle ###
# Format described at http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=mysql://root:12345@db:3306/reviewapi
###< doctrine/doctrine-bundle ###

###> symfony/swiftmailer-bundle ###
# For Gmail as a transport, use: "gmail://username:password@localhost"
# For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
# Delivery is disabled by default via "null://localhost"
MAILER_URL=null://localhost
###< symfony/swiftmailer-bundle ###

###> nelmio/cors-bundle ###
CORS_ALLOW_ORIGIN=^https?://localhost:?[0-9]*$
###< nelmio/cors-bundle ###

MYSQL_PASSWORD=123
MYSQL_ROOT_PASSWORD=12345
MYSQL_DATABASE=reviewapi
MYSQL_USER=user
``` 

### Installation steps
#### Docker
**Note:** To make proper `.env` files from `.env.dist` according to samples above.

1. Building containers 
    ```
    docker-compose build
    ```
2. Starting containers
    ```
    docker-compose up -d
    ```
Expecting successfully running containers via command `docker-compose ps`, for example:
```
    Name                   Command               State                 Ports               
-------------------------------------------------------------------------------------------
reviews-api     docker-php-entrypoint php-fpm    Up      9000/tcp                          
reviews-db      docker-entrypoint.sh mysqld      Up      0.0.0.0:33061->3306/tcp, 33060/tcp
reviews-nginx   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8080->80/tcp  
```
#### Symfony deps installation
It can be triggered with command:
```
docker-compose exec api php -d memory_limit=-1 /usr/local/bin/composer install
```
#### Symfony migrations applying
**Note:** It's interactive, to type `y` is required.
```
docker-compose exec api php bin/console doctrine:migrations:migrate
```

#### Symfony fake data fixtures applying to db - NOT RECOMMENDED!
**Note:** This is NOT RECOMMENDED!

Because of transactions it takes ages to complete 100K records inserted into DB.
```
docker-compose exec api php bin/console doctrine:fixtures:load
```
#### Symfony fake data via custom command - !RECOMMENDED!
**Note:** This is !RECOMMENDED!
```
docker-compose exec api php bin/console doctrine:fixtures:create-hotel-reviews
```
Output is running quite fast and takes a minute to complete:
```
! [NOTE] 0                                                                                                             

 ! [NOTE] 1000                                                                                                          

 ! [NOTE] 2000                                                                                                          

 ! [NOTE] 3000                                                                                                          

 ! [NOTE] 4000                                                                                                          

 ! [NOTE] 5000                                                                                                          

...

 ! [NOTE] 97000                                                                                                         

 ! [NOTE] 98000                                                                                                         

 ! [NOTE] 99000                                                                                                         

                                                                                                                        
 [OK] All data was generated                                                                                            
                                                                                                                        

                                                                                                                        
 [OK] And applied to DB   
```
## Usage
### Swagger UI
Swagger UI should be installed as deps package. Swagger schema is generated in OpenApi 3 format.

By default it's url is: [http://localhost:8080/docs/]()

There is complete description for all endpoints available.

Swagger configuration schema is also available directly:
* in JSON: [http://localhost:8080/docs/file/_swagger/swagger.json]()
* in YAML: [http://localhost:8080/docs/file/_swagger/swagger.yml]()

### Swagger files update
Swagger files may be updated manualy after valuable changes in API:

Json:
```
docker-compose exec api ./vendor/bin/openapi --pattern "/\.php$/" --format yaml -o ./docs/_swagger/swagger.yml ./src/Controller/ ./src/DTO/
```
Yaml:
```
docker-compose exec api ./vendor/bin/openapi --pattern "/\.php$/" --format json -o ./docs/_swagger/swagger.json ./src/Controller/ ./src/DTO/
```

## Tests
Tests are splitted into 3 types.

**Note:** First time running test will trigger composer extra deps installation. 

### Preparation setup
**Note:** For the first time it's required to make new test database, usually its expected to be `<database_name>_test` name format.

For example:
* to create DB
    ```
    docker-compose exec --env DATABASE_URL="mysql://root:12345@db:3306/reviewapi_test" api php bin/console doctrine:database:create --if-not-exists
    ```
**Note:** I'm providing db credentials explicitly to make sire it's `root` user with enough permissions to create new database

* to create tables
    ```
    docker-compose exec api php bin/console doctrine:migrations:migrate --env=test
    ```

### Unit-Tests
```
docker-compose exec api bin/phpunit ./tests/unit
```
Sample output:
```
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing ./tests/unit
............................                                      28 / 28 (100%)

Time: 1.83 seconds, Memory: 6.00 MB

OK (28 tests, 45 assertions)
```
### Integration-Tests
```
docker-compose exec api bin/phpunit ./tests/integration
```
Sample output:
```
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing ./tests/integration
..................................................                50 / 50 (100%)

Time: 1 minute, Memory: 44.50 MB

OK (50 tests, 211 assertions)
```

### Behavioral/Endpoint-Tests
```
docker-compose exec api bin/phpunit ./tests/api
```
Sample output:
```
PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing ./tests/api
.....................................................             53 / 53 (100%)

Time: 57.05 seconds, Memory: 58.00 MB

OK (53 tests, 91 assertions)
```
